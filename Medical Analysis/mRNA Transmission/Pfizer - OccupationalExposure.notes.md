

Notes:

pg.26

The development of an RNA-based vaccine encoding a viral antigen, which is then expressed by the vaccine recipient as a protein capable of eliciting protective immune responses, provides significant advantages over more traditional vaccine approaches. Unlike live attenuated vaccines, RNA vaccines do not carry the risks associated with infection and may be given to people who cannot be administered live virus (eg, pregnant women and immunocompromised persons). RNA-based vaccines are manufactured via a cell-free in vitro transcription process, which allows an easy and rapid production and the prospect of producing high numbers of vaccination doses within a shorter time period than achieved with traditional vaccine approaches. This capability is pivotal to enable the most effective response in outbreak scenarios.

